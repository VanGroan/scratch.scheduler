﻿using Scratch.Timer.Jobs;
using Scratch.Timer.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Scratch.Timer.Services
{
    public class GreeterJob : IScheduledJob
    {
        readonly private Greeter _greeter;

        public GreeterJob(Greeter greeter)
        {
            _greeter = greeter;
        }

        public string Schedule => "*/1 * * * *";

        public Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _greeter.SayHello();

            return Task.CompletedTask;
        }
    }
}
