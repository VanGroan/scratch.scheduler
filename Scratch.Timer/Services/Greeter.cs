﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scratch.Timer.Services
{
    public class Greeter
    {
        public void SayHello()
        {
            Console.WriteLine("************** Hello World ******************");
        }
    }
}
