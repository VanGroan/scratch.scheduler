﻿using Microsoft.Extensions.Hosting;
using NCrontab;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Scratch.Timer.Schedule
{
    public class SchedulerHostedService : HostedService
    {
        public event EventHandler<UnobservedTaskExceptionEventArgs> UnobservedTaskException;

        readonly private IList<SchedulerJobWrapper> _jobs = new List<SchedulerJobWrapper>();

        public SchedulerHostedService(IEnumerable<IScheduledJob> scheduledJobs)
        {
            var referenceTime = DateTime.UtcNow;

            foreach (var scheduledJob in scheduledJobs)
            {
                _jobs.Add(new SchedulerJobWrapper
                {
                    Schedule = CrontabSchedule.Parse(scheduledJob.Schedule),
                    Job = scheduledJob,
                    NextRunTime = referenceTime
                });
            }
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await ExecuteOnceAsync(cancellationToken);

                await Task.Delay(TimeSpan.FromMinutes(1), cancellationToken);
            }
        }

        private async Task ExecuteOnceAsync(CancellationToken cancellationToken)
        {
            var taskFactory = new TaskFactory(TaskScheduler.Current);
            var referenceTime = DateTime.UtcNow;

            var jobsThatShouldRun = _jobs.Where(t => t.ShouldRun(referenceTime)).ToList();

            foreach (var jobThatShouldRun in jobsThatShouldRun)
            { 
                jobThatShouldRun.Increment();

                await taskFactory.StartNew(
                    async () =>
                    {
                        try
                        {
                            await jobThatShouldRun.Job.ExecuteAsync(cancellationToken);
                        }
                        catch (Exception ex)
                        {
                            var args = new UnobservedTaskExceptionEventArgs(
                                ex as AggregateException ?? new AggregateException(ex));

                            UnobservedTaskException?.Invoke(this, args);

                            if (!args.Observed)
                            {
                                throw;
                            }
                        }
                    },
                    cancellationToken);
            }
        }

        public class SchedulerJobWrapper
        {
            public CrontabSchedule Schedule { get; set; }
            public IScheduledJob Job { get; set; }
            public DateTime LastRunTime { get; set; }
            public DateTime NextRunTime { get; set; }

            public void Increment()
            {
                LastRunTime = NextRunTime;
                NextRunTime = Schedule.GetNextOccurrence(NextRunTime);
            }

            public bool ShouldRun(DateTime currentTime)
            {
                return NextRunTime < currentTime && LastRunTime != NextRunTime;
            }
        }
    }
}
