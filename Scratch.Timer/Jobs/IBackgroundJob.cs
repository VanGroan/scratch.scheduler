﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scratch.Timer.Jobs
{
    public interface IBackgroundJob
    {
        Task Execute();
    }
}
