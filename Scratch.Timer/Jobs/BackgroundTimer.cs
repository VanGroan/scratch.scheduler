﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Scratch.Timer.Jobs
{
    public class BackgroundTimer<TJob> : BackgroundService where TJob : IBackgroundJob
    {
        readonly private TimeSpan _interval = TimeSpan.FromSeconds(5);
        readonly private IServiceScopeFactory _scopeFactory;

        public BackgroundTimer(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Task.Run(async () =>
            {
                do
                {
                    await Task.Delay(_interval);

                    using (var scope = _scopeFactory.CreateScope())
                    {
                        try
                        {
                            var job = scope.ServiceProvider.GetRequiredService<TJob>();
                            await job.Execute();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }

                    }
                    stoppingToken.ThrowIfCancellationRequested();
                } while (!stoppingToken.IsCancellationRequested);
            });

            return Task.CompletedTask;
        }
    }
}
